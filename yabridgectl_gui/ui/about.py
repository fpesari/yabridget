# SPDX-License-Identifier: GPL-3.0-or-later

from PyQt5 import QtCore, QtGui, QtWidgets

from .. import version
from .util import _translate, resource


class Ui_aboutDialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName('aboutDialog')
        Dialog.resize(346, 360)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            Dialog.sizePolicy().hasHeightForWidth())
        Dialog.setSizePolicy(sizePolicy)
        Dialog.setMinimumSize(QtCore.QSize(346, 360))
        Dialog.setMaximumSize(QtCore.QSize(346, 360))
        logo = resource('data/icons/logo.png')
        icon = QtGui.QIcon()
        icon.addPixmap(
            QtGui.QPixmap(logo), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName('gridLayoutA')
        self.frame = QtWidgets.QFrame(Dialog)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName('frameA')
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame)
        self.gridLayout_2.setObjectName('gridLayout_2A')
        self.labelLogo = QtWidgets.QLabel(self.frame)
        self.labelLogo.setMinimumSize(QtCore.QSize(100, 100))
        self.labelLogo.setMaximumSize(QtCore.QSize(100, 100))
        self.labelLogo.setText('')
        self.labelLogo.setPixmap(QtGui.QPixmap(logo))
        self.labelLogo.setScaledContents(True)
        self.labelLogo.setObjectName('labelLogo')
        self.gridLayout_2.addWidget(
            self.labelLogo, 0, 0, 1, 1, QtCore.Qt.AlignHCenter)
        self.labelText = QtWidgets.QLabel(self.frame)
        self.labelText.setTextFormat(QtCore.Qt.RichText)
        self.labelText.setAlignment(QtCore.Qt.AlignCenter)
        self.labelText.setWordWrap(True)
        self.labelText.setOpenExternalLinks(True)
        self.labelText.setObjectName('labelText')
        self.gridLayout_2.addWidget(
            self.labelText, 1, 0, 1, 1,
            QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.gridLayout.addWidget(self.frame, 0, 0, 1, 1)
        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate('aboutDialog', 'About'))
        self.labelText.setText(_translate('aboutDialog', '<html><head/><body><p>yabridget {} - Qt-based GUI for yabridgectl</p><p>This is libre software, released under the GNU GPLv3+.</p><p>yabridget was developed as part of the <a href="https://geekosdaw.tuxfamily.org/en/"><span style=" text-decoration: underline; color:#0000ff;">GeekosDAW</span></a> project, a FLOSS repository for music-making programs aimed at professional use.</p></body></html>'.format(version.__version__)))
