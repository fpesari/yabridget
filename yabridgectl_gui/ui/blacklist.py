# SPDX-License-Identifier: GPL-3.0-or-later

import os

from PyQt5 import QtCore, QtGui, QtWidgets

from .. import yabridgectl
from .util import _translate, resource


class Ui_blacklistDialog(object):
    def setupUi(self, blacklistDialog):
        self.blacklistDialog = blacklistDialog
        blacklistDialog.setObjectName('blacklistDialog')
        blacklistDialog.resize(400, 300)
        iconLogo = QtGui.QIcon()
        iconLogo.addPixmap(
            QtGui.QPixmap(resource('data/icons/logo.png')),
            QtGui.QIcon.Normal, QtGui.QIcon.Off)
        blacklistDialog.setWindowIcon(iconLogo)
        self.gridLayout = QtWidgets.QGridLayout(blacklistDialog)
        self.gridLayout.setObjectName('gridLayoutB')
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName('verticalLayoutB')
        self.listWidget = QtWidgets.QListWidget(blacklistDialog)
        self.listWidget.setObjectName('listWidgetB')
        self.initialize()
        self.verticalLayout.addWidget(self.listWidget)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName('horizontalLayoutB')
        self.frame = QtWidgets.QFrame(blacklistDialog)
        self.frame.setEnabled(False)
        self.frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame.setFrameShadow(QtWidgets.QFrame.Plain)
        self.frame.setLineWidth(0)
        self.frame.setObjectName('frameB')
        self.horizontalLayout.addWidget(self.frame)
        self.addButton = QtWidgets.QToolButton(blacklistDialog)
        self.addButton.setText('')
        self.addButton.clicked.connect(lambda: self.blacklist_add())
        iconAdd = QtGui.QIcon()
        iconAdd.addPixmap(
            QtGui.QPixmap(resource('data/icons/list-add.png')),
            QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.addButton.setIcon(iconAdd)
        self.addButton.setObjectName('addButton')
        self.horizontalLayout.addWidget(self.addButton)
        self.removeButton = QtWidgets.QToolButton(blacklistDialog)
        self.removeButton.setText('')
        self.removeButton.clicked.connect(lambda: self.blacklist_remove())
        iconRemove = QtGui.QIcon()
        iconRemove.addPixmap(
            QtGui.QPixmap(resource('data/icons/list-remove.png')),
            QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.removeButton.setIcon(iconRemove)
        self.removeButton.setObjectName('removeButton')
        self.horizontalLayout.addWidget(self.removeButton)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(blacklistDialog)
        QtCore.QMetaObject.connectSlotsByName(blacklistDialog)

    def retranslateUi(self, blacklistDialog):
        blacklistDialog.setWindowTitle(
            _translate('blacklistDialog', 'yabridget - blacklist'))
        self.addButton.setToolTip(
            _translate('blacklistDialog', 'Add directory'))
        self.addButton.setAccessibleName(
            _translate('blacklistDialog', 'Add directory'))
        self.removeButton.setToolTip(
            _translate('blacklistDialog', 'Remove directory'))
        self.removeButton.setAccessibleName(
            _translate('blacklistDialog', 'Remove directory'))

    def initialize(self):
        directories = yabridgectl.blacklist.list_()
        for directory in directories:
            self.listWidget.addItem(directory)

    def refresh(self):
        self.listWidget.clear()
        self.initialize()

    def blacklist_add(self):
        directory = QtWidgets.QFileDialog.getExistingDirectory(
                None,
                _translate('blacklistDialog', 'Select directory...'),
                os.path.expanduser('~'),
                QtWidgets.QFileDialog.ShowDirsOnly)
        if directory:
            yabridgectl.blacklist.add(directory)
            self.refresh()

    def blacklist_remove(self):
        current_item = self.listWidget.currentItem()
        if not current_item:
            return
        yabridgectl.blacklist.rm(current_item.text())
        self.listWidget.takeItem(self.listWidget.currentRow())
        self.refresh()
