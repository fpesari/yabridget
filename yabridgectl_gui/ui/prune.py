# SPDX-License-Identifier: GPL-3.0-or-later

from PyQt5 import QtCore, QtWidgets


class Ui_PruneDialog(object):
    def setupUi(self, Dialog, removed):
        Dialog.setObjectName('PruneDialog')
        Dialog.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName('verticalLayout')
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setObjectName('label')
        self.verticalLayout.addWidget(self.label)
        self.listWidget = QtWidgets.QListWidget(Dialog)
        self.listWidget.setObjectName('listWidget')
        self.verticalLayout.addWidget(self.listWidget)
        for i in removed:
            self.listWidget.addItem(i[2:])

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate('Dialog', 'yabridget - prune'))
        self.label.setText(_translate('Dialog', 'Leftover files removed:'))
