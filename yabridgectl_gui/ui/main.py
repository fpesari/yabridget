# SPDX-License-Identifier: GPL-3.0-or-later

import os
import sys

from PyQt5 import QtCore, QtGui, QtWidgets

from .. import yabridgectl
from . import about, blacklist, prune, status
from .util import _translate, resource


class FinishedSignal(QtCore.QObject):
    finished = QtCore.pyqtSignal(object)


class StatusWorker(QtCore.QRunnable):
    def __init__(self, prune):
        super(StatusWorker, self).__init__()
        self.prune = prune
        self.signals = FinishedSignal()

    @QtCore.pyqtSlot()
    def run(self):
        output = yabridgectl.main.sync(self.prune)
        self.signals.finished.emit(output)


class Ui_mainWindow(object):
    def setupUi(self, mainWindow):
        self.mainWindow = mainWindow
        self.threadpool = QtCore.QThreadPool()
        mainWindow.setObjectName('mainWindow')
        mainWindow.resize(800, 600)
        iconLogo = QtGui.QIcon()
        iconLogo.addPixmap(
            QtGui.QPixmap(resource('data/icons/logo.png')),
            QtGui.QIcon.Normal, QtGui.QIcon.Off)
        mainWindow.setWindowIcon(iconLogo)
        self.closeAction = QtWidgets.QShortcut(
            QtGui.QKeySequence('Ctrl+q'), mainWindow)
        self.closeAction.activated.connect(lambda: mainWindow.close())
        self.centralWidget = QtWidgets.QWidget(mainWindow)
        self.centralWidget.setObjectName('centralWidget')
        self.gridLayout = QtWidgets.QGridLayout(self.centralWidget)
        self.gridLayout.setObjectName('gridLayout')
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName('verticalLayout')
        self.aboutButton = QtWidgets.QToolButton(self.centralWidget)
        self.aboutButton.setObjectName('aboutButton')
        self.aboutButton.setMaximumSize(QtCore.QSize(15, 15))
        self.aboutButton.clicked.connect(self.show_about)
        self.verticalLayout.addWidget(
            self.aboutButton, 0, QtCore.Qt.AlignRight | QtCore.Qt.AlignTop)
        self.listWidget = QtWidgets.QListWidget(self.centralWidget)
        self.listWidget.setObjectName('listWidget')
        self.initialize()
        self.verticalLayout.addWidget(self.listWidget)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName('horizontalLayout')
        self.frame = QtWidgets.QFrame(self.centralWidget)
        self.frame.setEnabled(False)
        self.frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame.setFrameShadow(QtWidgets.QFrame.Plain)
        self.frame.setLineWidth(0)
        self.frame.setObjectName('frame')
        self.horizontalLayout.addWidget(self.frame)
        self.syncButton = QtWidgets.QToolButton(self.centralWidget)
        self.syncButton.setText('')
        self.syncButton.clicked.connect(self.sync_directories)
        iconSync = QtGui.QIcon()
        iconSync.addPixmap(
            QtGui.QPixmap(resource('data/icons/edit-find.png')),
            QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.syncButton.setIcon(iconSync)
        self.syncButton.setObjectName('syncButton')
        self.syncButton.setFocus(True)
        self.horizontalLayout.addWidget(self.syncButton)
        self.pruneCheckBox = QtWidgets.QCheckBox(self.centralWidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.pruneCheckBox.sizePolicy().hasHeightForWidth())
        self.pruneCheckBox.setSizePolicy(sizePolicy)
        self.pruneCheckBox.setAutoFillBackground(False)
        self.pruneCheckBox.setChecked(True)
        self.pruneCheckBox.setObjectName('pruneCheckBox')
        self.horizontalLayout.addWidget(self.pruneCheckBox)
        self.statusButton = QtWidgets.QToolButton(self.centralWidget)
        self.statusButton.setText('')
        self.statusButton.clicked.connect(self.show_status)
        iconStatus = QtGui.QIcon()
        iconStatus.addPixmap(
            QtGui.QPixmap(resource('data/icons/mail-signed-verified.png')),
            QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.statusButton.setIcon(iconStatus)
        self.statusButton.setObjectName('statusButton')
        self.horizontalLayout.addWidget(self.statusButton)
        self.blacklistButton = QtWidgets.QToolButton(self.centralWidget)
        self.blacklistButton.setText('')
        self.blacklistButton.clicked.connect(self.show_blacklist)
        self.blacklistButton.setObjectName('blacklistButton')
        iconBlacklist = QtGui.QIcon()
        iconBlacklist.addPixmap(
            QtGui.QPixmap(resource('data/icons/folder-locked.png')),
            QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.blacklistButton.setIcon(iconBlacklist)
        self.horizontalLayout.addWidget(self.blacklistButton)
        self.refreshButton = QtWidgets.QToolButton(self.centralWidget)
        self.refreshButton.setText('')
        self.refreshButton.clicked.connect(self.refresh)
        iconRefresh = QtGui.QIcon()
        iconRefresh.addPixmap(
            QtGui.QPixmap(resource('data/icons/view-refresh.png')),
            QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.refreshButton.setIcon(iconRefresh)
        self.refreshButton.setObjectName('refreshButton')
        self.horizontalLayout.addWidget(self.refreshButton)
        spacerItem = QtWidgets.QSpacerItem(
            5, 20, QtWidgets.QSizePolicy.Fixed,
            QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.addButton = QtWidgets.QToolButton(self.centralWidget)
        self.addButton.setText('')
        self.addButton.clicked.connect(self.add_directory)
        iconAdd = QtGui.QIcon()
        iconAdd.addPixmap(
            QtGui.QPixmap(resource('data/icons/list-add.png')),
            QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.addButton.setIcon(iconAdd)
        self.addButton.setObjectName('addButton')
        self.horizontalLayout.addWidget(self.addButton)
        self.removeButton = QtWidgets.QToolButton(self.centralWidget)
        self.removeButton.setText('')
        self.removeButton.clicked.connect(self.remove_directory)
        iconRemove = QtGui.QIcon()
        iconRemove.addPixmap(
            QtGui.QPixmap(resource('data/icons/list-remove.png')),
            QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.removeButton.setIcon(iconRemove)
        self.removeButton.setObjectName('removeButton')
        self.horizontalLayout.addWidget(self.removeButton)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)
        mainWindow.setCentralWidget(self.centralWidget)
        self.statusBar = QtWidgets.QStatusBar(mainWindow)
        self.statusBar.setObjectName('statusBar')
        mainWindow.setStatusBar(self.statusBar)
        self.clickable = (
            self.aboutButton, self.addButton, self.removeButton,
            self.refreshButton, self.statusButton, self.syncButton,
            self.blacklistButton, self.pruneCheckBox)

        self.retranslateUi(mainWindow)
        QtCore.QMetaObject.connectSlotsByName(mainWindow)

    def retranslateUi(self, mainWindow):
        mainWindow.setWindowTitle(
            _translate('mainWindow', 'yabridget'))
        self.syncButton.setToolTip(
            _translate('mainWindow', 'Scan for plugins'))
        self.syncButton.setAccessibleName(
            _translate('mainWindow', 'Scan for plugins'))
        self.pruneCheckBox.setToolTip(
            _translate('mainWindow', 'Prune when synchronizing'))
        self.pruneCheckBox.setAccessibleName(
            _translate('mainWindow', 'Prune when synchronizing'))
        self.pruneCheckBox.setText(_translate('mainWindow', 'Prune'))
        self.refreshButton.setToolTip(
            _translate('mainWindow', 'Refresh directories'))
        self.refreshButton.setAccessibleName(
            _translate('mainWindow', 'Refresh directories'))
        self.blacklistButton.setToolTip(
            _translate('mainWindow', 'Blacklist...'))
        self.blacklistButton.setAccessibleName(
            _translate('mainWindow', 'Blacklist...'))
        self.statusButton.setToolTip(
            _translate('mainWindow', 'Show status'))
        self.statusButton.setAccessibleName(
            _translate('mainWindow', 'Show status'))
        self.addButton.setToolTip(
            _translate('mainWindow', 'Add directory'))
        self.addButton.setAccessibleName(
            _translate('mainWindow', 'Add directory'))
        self.removeButton.setToolTip(
            _translate('mainWindow', 'Remove directory'))
        self.removeButton.setAccessibleName(
            _translate('mainWindow', 'Remove directory'))
        self.aboutButton.setStatusTip(_translate('mainWindow', 'About...'))
        self.aboutButton.setAccessibleName(
            _translate('mainWindow', 'About...'))
        self.aboutButton.setText(_translate('mainWindow', '?'))

    def initialize(self):
        directories = yabridgectl.main.list_()
        for directory in directories:
            self.listWidget.addItem(directory)

    def refresh(self):
        self.listWidget.clear()
        self.initialize()

    def add_directory(self):
        directory = QtWidgets.QFileDialog.getExistingDirectory(
                None,
                _translate('mainWindow', 'Select directory...'),
                os.path.expanduser('~'),
                QtWidgets.QFileDialog.ShowDirsOnly)
        if directory:
            yabridgectl.main.add(directory)
            self.refresh()
            self.statusBar.showMessage(
                _translate(
                    'mainWindow',
                    'Directory added. Press the Scan button when ready.'),
                10000)

    def remove_directory(self):
        current_item = self.listWidget.currentItem()
        if not current_item:
            return
        yabridgectl.main.rm(current_item.text(), self.ask_remove())
        self.listWidget.takeItem(self.listWidget.currentRow())
        self.refresh()

    def _sync(self, output):
        removed = tuple(
            filter(lambda x: x.startswith('-'),
                   output.stdout.split("\n")[4:]))
        message = output.stdout.split("\n")[-2]
        if output.stderr:
            message = output.stderr.split("\n")[0]
        self.statusBar.showMessage(message, 10000)
        if self.pruneCheckBox.isChecked() and removed:
            dialog = QtWidgets.QDialog(self.mainWindow)
            ui = prune.Ui_PruneDialog()
            ui.setupUi(dialog, removed)
            dialog.show()
        for i in self.clickable:
            i.setEnabled(True)

    def sync_directories(self):
        self.statusBar.showMessage(
            _translate('mainWindow', 'Syncing, please wait...'), 10000)
        prune = self.pruneCheckBox.isChecked()
        for i in self.clickable:
            i.setEnabled(False)
        status_worker = StatusWorker(prune)
        status_worker.signals.finished.connect(self._sync)
        self.threadpool.start(status_worker)

    def ask_remove(self):
        reply = QtWidgets.QMessageBox.question(
            self.mainWindow,
            _translate('mainWindow',
                       'Delete'),
            _translate('mainWindow',
                       'Do you want to delete any existing .so files?'),
            QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
            QtWidgets.QMessageBox.No)
        if reply == QtWidgets.QMessageBox.Yes:
            return True
        else:
            return False

    def show_about(self):
        dialog = QtWidgets.QDialog(self.mainWindow)
        ui = about.Ui_aboutDialog()
        ui.setupUi(dialog)
        dialog.show()

    def show_status(self):
        dialog = QtWidgets.QDialog(self.mainWindow)
        ui = status.Ui_statusDialog()
        ui.setupUi(dialog)
        dialog.show()

    def show_blacklist(self):
        dialog = QtWidgets.QDialog(self.mainWindow)
        ui = blacklist.Ui_blacklistDialog()
        ui.setupUi(dialog)
        dialog.show()


def run():
    app = QtWidgets.QApplication(sys.argv)
    main_window = QtWidgets.QMainWindow()
    ui = Ui_mainWindow()
    ui.setupUi(main_window)
    main_window.show()
    sys.exit(app.exec_())
