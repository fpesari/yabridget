# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path, PurePath

from PyQt5 import QtCore

_translate = QtCore.QCoreApplication.translate


def resource(path):
    top_dir = Path(__file__).resolve().parent
    return str(PurePath(top_dir, path))
