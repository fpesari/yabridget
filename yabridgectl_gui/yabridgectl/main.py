# SPDX-License-Identifier: GPL-3.0-or-later

import subprocess

from .util import EXEC


def add(directory):
    return subprocess.run((EXEC, 'add', directory),
                          stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE,
                          universal_newlines=True).stderr


def rm(directory, remove_so=True):
    process = subprocess.Popen((EXEC, 'rm', directory),
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               universal_newlines=True)
    answer = 'YES'
    if not remove_so:
        answer = 'NO'
    process.communicate(answer)


def sync(prune=True):
    command = (EXEC, 'sync') + (('--prune',) if prune else ())
    return subprocess.run(command,
                          stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE,
                          universal_newlines=True)


def list_():
    return tuple(filter(None, subprocess.run(
        (EXEC, 'list'),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True).stdout.split("\n")))


def status():
    output = subprocess.run(
        (EXEC, 'status'),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True).stdout
    try:
        path, vst, vstt, _, method, _, *rest, _ = output.split("\n")
    except ValueError:
        path, vst, vstt, _, method, _ = output.split("\n")
        return locals()
    result = []
    _last_path = ''
    for i in rest:
        if not i.strip():
            continue
        if not i.startswith(' '):
            _last_path = i.strip()
            continue
        # Thanks to Robbert van der Helm for the proper splitting algorithm
        plugin, rest_ = (j.strip() for j in i.split(' :: '))
        attributes = rest_.split(', ')
        kind = attributes[0]
        legacy = ''
        if attributes[1] == 'legacy':
            legacy = '✓'
        arch = attributes[-2]
        method = attributes[-1]
        result.append((plugin, kind, legacy, arch, method, _last_path))
    return locals()
