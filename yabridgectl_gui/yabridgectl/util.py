# SPDX-License-Identifier: GPL-3.0-or-later

from os import path
from shutil import which


EXEC = 'yabridgectl'

LOCAL_PATH = path.join(
    path.expanduser('~'), '.local', 'share', 'yabridge')

if which(EXEC, path=LOCAL_PATH):
    EXEC = path.join(LOCAL_PATH, EXEC)
