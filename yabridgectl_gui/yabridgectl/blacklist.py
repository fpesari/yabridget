# SPDX-License-Identifier: GPL-3.0-or-later

import subprocess

from .util import EXEC


def add(directory):
    return subprocess.run((EXEC, 'blacklist', 'add', directory),
                          stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE,
                          universal_newlines=True).stderr


def rm(directory):
    return subprocess.Popen((EXEC, 'blacklist', 'rm', directory),
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            universal_newlines=True)


def list_():
    return tuple(filter(None, subprocess.run(
        (EXEC, 'blacklist', 'list'),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True).stdout.split("\n")))
