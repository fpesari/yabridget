all:
	python3 setup.py build

install:
	python3 setup.py install --user
	mkdir -p ~/.local/share/applications
	sed s:Exec=:Exec=~/.local/bin/: share/yabridget.desktop  > ~/.local/share/applications/yabridget.desktop
	mkdir -p ~/.local/share/icons
	cp share/yabridget.xpm ~/.local/share/icons

uninstall:
	python3 -m pip uninstall yabridget
	rm -f ~/.local/share/applications/yabridget.desktop
	rm -f ~/.local/share/icons/yabridget.xpm
