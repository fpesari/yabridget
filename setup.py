#!/usr/bin/python3

from pathlib import Path
from setuptools import setup

exec(Path('./yabridgectl_gui/version.py').read_text())

setup(version=__version__)
