# yabridget - Qt-based GUI for yabridgectl

`yabridget` is a simple Qt-based GUI for `yabridgectl`.

It was developed as part of the
[GeekosDAW](https://geekosdaw.tuxfamily.org/en/) project,
an openSUSE repository for libre music-making programs.

![Screenshot](share/screenshot.png "yabridget screenshot")

## Installation

`yabridget` needs `yabridgectl` in order to run (which, in turn,
depends on `yabridge`).

It also needs Python 3.8+ and its `setuptools` subpackage
(often called `python3-setuptools` in distro repositories).
Optionally, to make uninstallation easier, it also needs the
`pip` subpackage for `python3`.

To install it locally, download the latest tarball or clone this
repository, then move to the root directory and run either:

```
make install
```

for a full installation including the `.desktop` entry and
icon; else:

```
python3 setup.py install --user
```

to install just the Python package.

If you used the `make` install method, some desktop environments
will recognize user paths to discover local applications. If
yours doesn't, simply add to the `$PATH` environmental variable
`~/.local/bin` and launch `yabridget` from the command line
(or a launcher).

If you have `pip` for Python 3, you can uninstall `yabridget` by
running:

```
make uninstall
```

if you used the `make` install method; else by running:

```
python3 -m pip uninstall yabridget
```

## Usage

After launching `yabridget`, you have some buttons which are
mapped to `yabridgectl` functions: refresh (list), sync, add and
rm. Simply click the button corresponding to the option you are
interested in.

## Design notes

Rather than work on the `.toml` config file, this GUI calls
directly the command-line `yabridgectl` program because it is
more predictable than an internal configuration format.

## License

Every original file in this repository is released under the
GNU General Public License, version 3 or any later version,
unless otherwise specified (and even in that case, it's all free
software/data).

For additional licenses, see the `licenses/` directory.
